FROM ruby:2.5-alpine

LABEL maintainer="Sven Erik Rebane <svenerik.rebane@gmail.com>"

LABEL Name=ajateen.io Version=0.0.1

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /backend-app
COPY ./backend/. ./

COPY ./backend/Gemfile.lock ./backend/Gemfile ./

RUN apk add --update \
    build-base \
    linux-headers \
    git \
    postgresql-dev \
    nodejs \
    yarn \
    tzdata \
    graphviz \
    gmp-dev \
    bash

RUN gem install bundler -v 2.0.2 && bundle install --jobs 20
RUN rails app:update:bin

CMD cd backend && rm -f tmp/pids/server.pid && bundle exec rails s -p 20190 -b '0.0.0.0'

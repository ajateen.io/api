Rails.application.routes.draw do
  resources :sessions, only: [:create, :destroy, :current_user]
  resources :groups, only: [:index, :show, :update, :new, :destroy]
  resources :activity_categories, only: [:index, :show, :update, :new, :destroy]
  resources :activities, only: [:index, :show, :update, :new, :destroy]
  resources :ranks, only: [:index]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

class ActivityCategoriesController < ApplicationController
  def index
    @activity_categories = ActivityCategory.all
    render :json => @activity_categories
  end

  def show
    @activity_category = ActivityCategory.find(params[:id])
    render :json => @activity_category
  end
  def update
    @activity_category = ActivityCategory.find(params[:activity][:id])
    @activity_category.name_en = params[:activity_category][:name_en]
    @activity_category.save

    render :json => @activity_category
  end

  def new
    @activity_category = ActivityCategory.create(
      name_en: params[:activity_category][:name_en],
    )

    render :json => @activity_category
  end

  def destroy
    @activity_category  = ActivityCategory.find(params[:id]).destroy
    render :json => @activity_category
  end
end

require 'date'

class GroupsController < ApplicationController
  def index
    @q = Group.ransack(params[:q])
    @groups = @q.result(distinct: true)

    render :json => @groups.map{|group| {
      :id => group.id,
      :title => group.activity.name_en,
      :activity_id => group.activity.id,
      :start_time => group.start_time,
      :end_time => group.end_time,
      :participants => group.member_count,
      :max_participants => group.max_participants,
      :location => group.location,
      :created_at => group.created_at,
      :updated_at => group.updated_at}
    }.to_json
  end

  def show
    @group = Group.find(params[:id])
    render :json => @group, :include => [:activity, :soldiers], :except => [:activity_id]
  end

  def update
    @group = Group.find(params[:group][:id])
    @group.activity = params[:group][:activity]
    @group.start_time = params[:group][:start_time]
    @group.end_time = params[:group][:end_time]
    @group.max_participants = params[:group][:max_participants]
    @group.location = params[:location][:location]

    @group.save

    render :json => @group
  end

  def new
    @group = Group.create(
      activity: params[:group][:activity],
      start_time: params[:group][:start_time],
      end_time: params[:group][:end_time],
      max_participants: params[:group][:max_participants],
      location: params[:location][:location]
    )

    render :json => @group
  end

  def destroy
    @group = Group.find(params[:id]).destroy
    render :json => @group
  end
end

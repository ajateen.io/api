class SessionsController < ApplicationController
  acts_as_token_authentication_handler_for User, fallback_to_devise: false
  skip_before_action :auth_token, :only => [:create]

  def create
    user = User.valid_login? params[:email], params[:password]
    if user
      render :json => user, :include => [:soldier], :only => [:email, :authentication_token], status: :ok
    else
      head :unauthorized
    end
  end

  def destroy
    if Soldier.find(params[:id]) == @current_user.soldier
      @current_user.reset_authentication_token!
      head :ok
    else
      head :unauthorized
    end
  end
end

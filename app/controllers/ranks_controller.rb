class RanksController < ApplicationController
  def index
    @ranks = Rank.all
    render :json => @ranks.map{|rank| {:id => rank.id, :name => rank.name, :tag => rank.tag_name, :created_at => rank.created_at, :updated_at => rank.updated_at}}.to_json
  end
end

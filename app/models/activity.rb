class Activity < ApplicationRecord
    belongs_to :category, class_name: 'ActivityCategory', foreign_key: 'category_id'

    has_many :activity_subscriptions
    has_many :subscribers, :through => :activity_subscriptions
    has_many :groups

    validates :category, presence: true
    validates :name_en, presence: true
end

class Rank < ApplicationRecord
    has_many :soldier
    attr_accessor :name

    def name
        I18n.translate("rank.#{self.tag_name}")
    end
end

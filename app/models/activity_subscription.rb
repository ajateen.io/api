class ActivitySubscription < ApplicationRecord
  belongs_to :soldier
  belongs_to :activity
end

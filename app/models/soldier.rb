class Soldier < ApplicationRecord
  belongs_to :rank
  belongs_to :superior, class_name: 'Soldier', foreign_key: 'superior_id', optional: true

  has_many :activity_subscriptions
  has_many :subscriptions, :through => :activity_subscriptions
  has_many :group_members
  has_many :groups, :through => :group_members
  has_one :user

  validates :rank, presence: true

  def full_name
    first_name + last_name
  end

  def self.system_soldier
    Soldier.find_by_identity_code(00000000000)
  end
end

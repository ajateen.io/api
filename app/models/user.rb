class User < ApplicationRecord
  has_secure_password
  before_create :generate_token

  belongs_to :soldier

  def generate_token
    self.authentication_token = generate_auth_token
  end

  def reset_authentication_token!
    authentication_token_will_change!
    
    self.authentication_token = generate_auth_token
    self.save
  end

  def not_system_user?
    self != User.system_user
  end

  def auth key
    authentication_token == key || self.authenticate(key) ? self : false
  end

  def self.valid_token? email, token
    user = User.find_by_email(email)

    return user && user.auth(token) && user.not_system_user? ? user : false
  end

  def self.valid_login? email, password
    user = User.find_by_email(email)

    return user && user.auth(password) && user.not_system_user? ? user : false
  end

  def self.system_user
    User.find_by_soldier_id(Soldier.system_soldier.id)
  end

  private
  def generate_auth_token
    return loop do
      token = SecureRandom.urlsafe_base64(nil, false)
      break token unless User.exists?(authentication_token: token)
    end
  end
end

class CreateGroupMembers < ActiveRecord::Migration[5.2]
  def change
    create_table :group_members do |t|
      t.integer :soldier_id
      t.integer :group_id

      t.timestamps
    end
  end
end

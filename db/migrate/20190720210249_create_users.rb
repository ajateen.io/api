class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.integer :soldier_id
      t.string :email
      t.string :password_digest
      t.string :authentication_token, limit: 30

      t.timestamps
    end
    add_index :users, :authentication_token, unique: true
  end
end

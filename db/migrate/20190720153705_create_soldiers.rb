class CreateSoldiers < ActiveRecord::Migration[5.2]
  def change
    create_table :soldiers do |t|
      t.string :first_name
      t.string :last_name
      t.bigint :identity_code
      t.integer :rank_id
      t.integer :superior_id, null: true

      t.timestamps
    end
  end
end

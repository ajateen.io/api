class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|
      t.integer :activity_id
      t.datetime :start_time
      t.datetime :end_time
      t.integer :max_participants
      t.string :location

      t.timestamps
    end
  end
end

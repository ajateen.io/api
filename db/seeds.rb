# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'date'

### Create ranks ###
ranks = ["RMS", "KPR", "N-SRS", "SRS", "V-SRS", "N-VLB", "VBL", "V-VBL", "ST-VBL", "Ü-VBL", "LPN", "N-LTN", "LTN", "KPT", "MJR", "KOL-LTN", "KOL", "BRIG-KIN", "KIN-MJR", "KIN-LTN", "KIN"]

ranks.each do |rank|
    if Rank.find_by_tag_name(rank).nil?
        Rank.create(tag_name: rank)
    end
end


### Create activity categories & activies ###
activity_categories = [
    ["sport", ["Football", "Volleyball", "Jogging", "Gym", "Boxing", "Basketball", "Crossfit", "Street Workout",]],
    ["Fun & Games", ["Billiard", "Table Tennis", "PS4", "LAN Party", "Board Games", "Movie Night", "Chillout", "Playing Cards"]],
    ["Recreational Activities", ["Cooking", "Jammin’ / Music", "Ar"]],
    ["Education & Knowledge", ["Language Class", "Equipment Maintenance"]]
]

activity_categories.each do |ac, aa|
    category = ActivityCategory.find_by_name_en(ac)
    if category.nil?
        category = ActivityCategory.create(name_en: ac)
    end

    aa.each do |a|
        if Activity.find_by_name_en(a).nil?
            Activity.create(name_en: a, category: category)
        end
    end
end

### Soldiers ###
soldiers = [["Madis Mets", 1, 47703110001], ["Kalle Kaasik", 1, 49905031114], ["Tobias Õunpuu", 1, 50111052432], ["Mari Maasikas", 1, 50006287564], ["Gustav Järv", 3, 48912302213], ["Henri Erikson", 3, 49909043312], ["Sven Juhto", 2, 49601143241], ["John Wester", 5, 50002128532], ["Paul Adamson", 4, 49009135903], ["Martin Roos", 1, 50310194837], ["Brigitte Sõel", 3, 50004247351], ["Berta Stevenson", 3, 49706044879], ["Jüri Tamm", 2, 49801018765], ["Keijo Murumaa", 1, 49505058914], ["Oskar Hett", 1, 50011034981], ["Peeter Lae", 1, 50204068673], ["Pierre Ruby", 4, 48707186924], ["Jarmo Sooke", 1, 50309108256], ["Mehis Teemant",	1, 49904206921], ["Neil Jackson", 3, 48904055348]]

soldiers.each do |name, rank, id_code|
  if Soldier.find_by_identity_code(id_code).nil?
    s = Soldier.create(
      first_name: name.split(' ')[0],
      last_name: name.split(' ')[1],
      rank: Rank.find(rank),
      identity_code: id_code
    )
  end
end


### Subscriptions ###
subscriptions = [["Football", ["Pierre Ruby", "Martin Roos", "Kalle Kaasik", "Mehis Teemant", "Jüri Tamm", "Henri Erikson"]], ["Boxing", ["Gustav Järv", "Mehis Teemant", "Jarmo Sooke", "Jüri Tamm"]], ["Crossfit", ["Mari Maasikas", "Brigitte Sõel", "Jüri Tamm", "Berta Stevenson"]], ["Art", ["Martin Roos", "Jarmo Sooke", "Berta Stevenson", "Jarmo Sooke", "Jüri Tamm"]], ["LAN Party", ["Henri Erikson", "Kalle Kaasik", "Martin Roos", "Keijo Murumaa"]], ["Volleyball", ["Berta Stevenson", "Brigitte Sõel", "Mari Maasikas", "Jüri Tamm", "Pierre Ruby"]]]

subscriptions.each do |activity, name_array|
  activity = Activity.find_by_name_en(activity)

  name_array.each do |name|
    soldier = Soldier.find_by(first_name: name.split(" ")[0], last_name: name.split(" ")[1])

    unless activity.nil? && soldier.nil?
      if ActivitySubscription.find_by(activity: activity, soldier: soldier).nil?

        error = ActivitySubscription.create(
          activity: activity,
          soldier: soldier
        )
      end
    end
  end
end


### Groups & Members ###
groups = [["Football",  DateTime.parse("19/07/20T16:00:00+02:00"),  DateTime.parse("19/07/20T19:00:00+02:00"), "Football field", 20, ["Jarmo Sooke", "John Wester", "Gustav Järv", "Tobias Õunpuu", "Madis Mets", "Keijo Murumaa", "Mehis Teemant", "Peeter Lae", "Pierre Ruby"]], ["Crossfit",  DateTime.parse("19/07/15T17:00:00+02:00"),  DateTime.parse("19/07/15T18:00:00+02:00"), "AH01", 15, ["Brigitte Sõel", "Berta Stevenson", "Paul Adamson", "Mari Maasikas", "Gustav Järv", "Madis Mets"]], ["Table Tennis", DateTime.parse("19/07/25T13:00:00+02:00"),  DateTime.parse("19/07/25T14:30:00+02:00"), "SK", 8, ["Oskar Hett", "Oskar Hett", "Pierre Ruby", "Henri Erikson", "Martin Roos", "Neil Jackson"]], ["LAN Party",  DateTime.parse("19/08/15T18:00:00+02:00"),  DateTime.parse("19/08/015T22:00:00+02:00"), "Roger", 8, ["Kalle Kaasik", "Mehis Teemant", "Oskar Hett", "Keijo Murumaa"]], ["Cooking",  DateTime.parse("19/09/09T10:00:00+02:00"),  DateTime.parse("19/09/09T12:00:00+02:00"), "Kitchen", 5, ["Berta Stevenson", "Brigitte Sõel", "Keijo Murumaa", "Neil Jackson"]],["Language Class",  DateTime.parse("19/08/20T14:00:00+02:00"),  DateTime.parse("19/08/20T16:00:00+02:00"), "Auditorium", 140, ["Henri Erikson", "John Wester", "Peeter Lae", "Mari Maasikas", "Jarmo Sooke"]]]

groups.each do |activity, start_time, end_time, location, max_participants, participants|
  activity = Activity.find_by_name_en(activity)
  if Group.find_by(activity: activity, start_time: start_time, end_time: end_time, max_participants: max_participants, location: location).nil?
    group = Group.create(activity: activity, start_time: start_time, end_time: end_time, max_participants: max_participants, location: location)
    participants.each do |name|
       GroupMember.create(
         soldier: Soldier.find_by(first_name: name.split(" ")[0], last_name: name.split(" ")[1]),
         group: group
       )
    end
  end
end

### Test User ###
soldier = Soldier.find_by_identity_code(39903100276)
if soldier.nil?
  soldier = Soldier.create(
    first_name: "Sven",
    last_name: "Rebane",
    identity_code: 39903100276,
    rank_id: 1
  )
end

if User.find_by_soldier_id(soldier.id).nil?
  User.create(
    soldier: soldier,
    email: "#{soldier.first_name.downcase}.#{soldier.last_name.downcase}",
    password_digest: "12345678"
  )
end

### System User ###
if Soldier.system_soldier.nil?
  Soldier.create(
    first_name: "System",
    last_name: "User",
    identity_code: 00000000000,
    rank_id: 21
  )
end

if User.system_user.nil?
  User.create(
    soldier: Soldier.system_soldier,
    email: "system.user",
    password_digest: "00000000",
  )
end
